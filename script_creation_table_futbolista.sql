CREATE TABLE posicion (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL
);

CREATE TABLE futbolista (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombres VARCHAR(100) NOT NULL,
    apellidos VARCHAR(100) NOT NULL,
    fecha_nacimiento DATE NOT NULL,
    caracteristicas TEXT,
    posicion_id INT,
    FOREIGN KEY (posicion_id) REFERENCES posicion(id)
);

INSERT INTO futbolista(nombres, apellidos, fecha_nacimiento, caracteristicas, posicion_id) 
VALUES 
('Sergio', 'Ramos', '1986-03-30', 'Buen defensor, líder', 2),
('Robert', 'Lewandowski', '1988-08-21', 'Goleador, buen rematador', 4),
('Kevin', 'De Bruyne', '1991-06-28', 'Excelente visión de juego, buen pasador', 2),
('Mohamed', 'Salah', '1992-06-15', 'Rápido, buen regate', 3),
('Manuel', 'Neuer', '1986-03-27', 'Bueno en el mano a mano, buen juego aéreo', 1),
('Lionel', 'Messi', '1987-06-24', 'Velocidad, regate, precisión en tiros libres', 4),
('Cristiano', 'Ronaldo', '1985-02-05', 'Fuerza física, remate, salto', 4),
('Neymar', 'Jr', '1992-02-05', 'Regate, habilidad técnica, visión de juego', 3),
('Manuel', 'Neuer', '1986-03-27', 'Agilidad, reflejos, juego de pies', 1),
('Virgil', 'van Dijk', '1991-07-08', 'Fuerza, marcaje, juego aéreo', 2),
('Luka', 'Modric', '1985-09-09', 'Gran control de balón, visión de juego', 2),
('Virgil', 'van Dijk', '1991-07-08', 'Imponente en el juego aéreo, buena lectura de juego', 2),
('Kylian', 'Mbappé', '1998-12-20', 'Velocidad, habilidad para driblar', 3),
('Kevin', 'Kurányi', '1982-03-02', 'Goleador, oportunista en el área', 4),
('Thiago', 'Silva', '1984-09-22', 'Experiencia, líder en defensa', 2),
('Marco', 'Reus', '1989-05-31', 'Habilidad con el balón, buen remate', 4),
('Harry', 'Kane', '1993-07-28', 'Excelente rematador, inteligente en el juego', 4),
('Eden', 'Hazard', '1991-01-07', 'Regate, visión de juego', 3);

INSERT INTO posicion (nombre) VALUES 
    ('Arquero'),
    ('Defensa'),
    ('Mediocampista'),
    ('Delantero');
select * from posicion;

select * from futbolista f inner join posicion p on f.posicion_id = p.id
