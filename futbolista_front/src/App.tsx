import React from 'react';
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import FutbolistaComponent  from './component/futbolistaComponent';


function App() {
  return (
      <BrowserRouter>
        <Routes>
          <Route path = '/' element={<FutbolistaComponent/>}></Route>
        </Routes>
      </BrowserRouter>
  );
}

export default App
