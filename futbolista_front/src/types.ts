// types.ts

export interface Futbolista {
  id: number;
  nombres: string;
  apellidos: string;
  fechaNacimiento: string;
  caracteristicas: string;
  posicionModel: {
    id: number;
    nombre: string;
  }
}


export interface Posicion {
  id: number;
  nombre: string;
}

