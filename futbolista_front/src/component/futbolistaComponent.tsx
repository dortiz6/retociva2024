import React from 'react';
import axios from 'axios'
import { useEffect, useState } from 'react';
import { Futbolista, Posicion } from '../types';
import { Col, Container, Form, InputGroup, Row, Stack, Table } from 'react-bootstrap';
import { Modal } from 'react-bootstrap';


const futbolistaComponent = () => {
  const url = "http://localhost:4000/futbolista";
  const [futbolistas, setListaFutbolistas] = useState<Futbolista[]>([]);
  const [search, setSearch] = useState('');
  const [paginaActual, setPaginaActual] = useState(1);
  const [mostrarModal, setMostrarModal] = useState(false);
  const [futbolista, setfutbolista] = useState<Futbolista>();


  useEffect(() => {
    getFutbolistas();
  }, [paginaActual]);

  const getFutbolistas = async () => {
    try {
      const respuesta = await axios.get(url);
      setListaFutbolistas(respuesta.data);
      console.log(respuesta.data)
    } catch (error) {
      console.error('Error al obtener Lista de futbolistas:', error);
    }
  }

  //Obtener Futbolista por Id
  const handleOpenModal = async (id) => {
    try {
      const respuesta = await axios.get(`${url}/${id}`);
      setfutbolista(respuesta.data);
      setMostrarModal(true);
    } catch (error) {
      console.error('Error al obtener el futbolista seleccionado:', error);
    }
  };


  //Paginacion
  const paginas = 10;
  const ultIndex = paginaActual * paginas;
  const priIndex = ultIndex - paginas;

  const buscarFutbolistas = futbolistas.filter((futbolista) => {
    return search.toLowerCase() === '' ? futbolista : futbolista.nombres.toLowerCase().includes(search.toLowerCase());
  });

  const totalPaginas = Math.ceil(buscarFutbolistas.length / paginas);

  const handlePaginaAnterior = () => {
    setPaginaActual((page) => page - 1);
  };

  const handlePaginaPosterior = () => {
    setPaginaActual((page) => page + 1);
  };

  return (
    <div className='container-fluid bg-dark bg-gradient text-white'>
      <div className="mask d-flex align-items-center h-100">
        <div className='container'>
          <div className='row'>
            <div className='col'>
              <div className="px-auto py-2 my-4 text-center">
                <h1 className='display-4 fw-bold'>Lista de Futbolistas</h1>
              </div>
            </div>
          </div>
          <Form>
            <InputGroup className='px-auto py-2 my-2 text-cente'>
              <Form.Control className='shadow-lg p-3 mb-4 bg-body rounded' onChange={(e) => setSearch(e.target.value)}
                placeholder='Buscar Futbolista' />
            </InputGroup>
          </Form>

          <div className="card shadow-2-strong shadow-lg p-3 mb-5 bg-body rounded">
            <div className="card-body table-responsive">
              <table className='table table-borderless mb-0'>
                <thead className="table-light">
                  <tr>
                    <th>Id</th>
                    <th>Nombres y Apellidos</th>
                    <th>Fecha Nacimiento</th>
                    <th>Características</th>
                    <th>Posición</th>
                  </tr>
                </thead>
                <tbody>{buscarFutbolistas
                  .slice(priIndex, ultIndex)
                  .map((futbolista) => (
                    <tr key={futbolista.id} onClick={() => handleOpenModal(futbolista.id)}>
                      <td>{futbolista.id}</td>
                      <td>{futbolista.nombres + " " + futbolista.apellidos}</td>
                      <td>{new Date(futbolista.fechaNacimiento).toLocaleDateString()}</td>
                      <td>{futbolista.caracteristicas}</td>
                      <td>{futbolista.posicionModel.nombre}</td>
                    </tr>
                  ))
                }
                </tbody>
              </table>
            </div>
          </div>

          <div className='text-center my-5 '>
            <button type="button" className="btn btn-outline-light"
              onClick={handlePaginaAnterior} disabled={paginaActual === 1}>
              Página anterior
            </button>
            <span className='mx-2'>
              Página {paginaActual} de {totalPaginas}
            </span>
            <button type="button" className="btn btn-outline-light"
              onClick={handlePaginaPosterior} disabled={paginaActual === totalPaginas}>
              Página siguiente
            </button>
          </div>

          <Modal show={mostrarModal} onHide={() => setMostrarModal(false)}>
            <Modal.Header closeButton>
              <Modal.Title>Detalles del Futbolista</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {futbolista && (
                <tr>
                  <p><strong>ID:</strong> {futbolista.id}</p>
                  <p><strong>Nombres:</strong> {futbolista.nombres}</p>
                  <p><strong>Apellidos:</strong> {futbolista.apellidos}</p>
                  <p><strong>Fecha de nacimiento:</strong> {new Date(futbolista.fechaNacimiento).toLocaleDateString()}</p>
                  <p><strong>Caracteristicas:</strong> {futbolista.caracteristicas}</p>
                  <p><strong>Posición:</strong> {`${futbolista.posicionModel.nombre} (${futbolista.posicionModel.id})`}</p>
                </tr>
              )}
            </Modal.Body>
            <Modal.Footer>
              <button type="button" className="btn btn-danger" onClick={() => setMostrarModal(false)}>Cerrar</button>
            </Modal.Footer>
          </Modal>
        </div>
      </div>
    </div>
  );
}

export default futbolistaComponent
