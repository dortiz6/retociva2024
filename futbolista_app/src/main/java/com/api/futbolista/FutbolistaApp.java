package com.api.futbolista;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FutbolistaApp {

	public static void main(String[] args) {
		SpringApplication.run(FutbolistaApp.class, args);
	}

}
