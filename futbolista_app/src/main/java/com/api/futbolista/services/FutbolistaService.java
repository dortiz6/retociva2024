package com.api.futbolista.services;

import java.util.ArrayList;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.futbolista.models.FutbolistaModel;
import com.api.futbolista.models.PosicionModel;
import com.api.futbolista.repositories.IFutbolistaRepository;
import com.api.futbolista.repositories.IPosicionRepository;

@Service
public class FutbolistaService {

    @Autowired
    IFutbolistaRepository futbolistaRepository;
    IPosicionRepository posicionRepository;

    public ArrayList<FutbolistaModel> getFutbolista() {
        return (ArrayList<FutbolistaModel>) futbolistaRepository.findAll();
    }

    public Optional<FutbolistaModel> getById(Long id) {
        return futbolistaRepository.findById(id);
    }

    public ArrayList<PosicionModel> getPosicion() {
        return (ArrayList<PosicionModel>) posicionRepository.findAll();
    }
}
