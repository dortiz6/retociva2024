package com.api.futbolista.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.futbolista.models.PosicionModel;

@Repository
public interface IPosicionRepository extends JpaRepository<PosicionModel, Long>{

}
