package com.api.futbolista.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.futbolista.models.FutbolistaModel;

@Repository
public interface IFutbolistaRepository extends JpaRepository<FutbolistaModel, Long>{

}
