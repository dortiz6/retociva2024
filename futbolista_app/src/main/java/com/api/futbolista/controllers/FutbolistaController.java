package com.api.futbolista.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.futbolista.services.FutbolistaService;

import ch.qos.logback.core.model.Model;

import com.api.futbolista.models.FutbolistaModel;
import com.api.futbolista.models.PosicionModel;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/futbolista")
public class FutbolistaController {

    @Autowired
    private FutbolistaService futbolistaService;

    @GetMapping
    public ArrayList<FutbolistaModel> getFutbolista() {
        return this.futbolistaService.getFutbolista();
    }

    @GetMapping(path = "/{id}")
    public Optional<FutbolistaModel> getFutbolistaById(@PathVariable("id") Long id) {
        return this.futbolistaService.getById(id);
    }

    @GetMapping(path = "/posicion")
    public ArrayList<PosicionModel> getPosicion() {
        return this.futbolistaService.getPosicion();
    }

}
