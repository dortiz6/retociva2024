package com.api.futbolista.models;

import java.io.Serializable;
import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "futbolista")
public class FutbolistaModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String nombres;
    @Column
    private String apellidos;
    @Column
    private Date fechaNacimiento;
    @Column
    private String caracteristicas;

    @ManyToOne
    @JoinColumn(name = "posicion_id")
    private PosicionModel posicionModel;

    public PosicionModel getPosicionModel() {
        return posicionModel;
    }

    public void setPosicionModel(PosicionModel posicionModel) {
        this.posicionModel = posicionModel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(String caracteristicas) {
        this.caracteristicas = caracteristicas;
    }



    public FutbolistaModel(Long id, String nombres, String apellidos, Date fechaNacimiento, String caracteristicas,
            PosicionModel posicionModel) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.fechaNacimiento = fechaNacimiento;
        this.caracteristicas = caracteristicas;
        this.posicionModel = posicionModel;
    }

    public FutbolistaModel() {
    }


}
